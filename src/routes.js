import { Dashboard } from 'modules';

const routes = [    
    {
        path: '/dashboards/:id',
        component: Dashboard,
        exact: true
    }
];

export default routes;