const sortLayout = layout => layout.sort((a, b) => a.id - b.id);

export const widgetsEqual = (widget, otherWidget, keys = ['i', 'w', 'h', 'x', 'y']) => {
    if (widget === otherWidget) { return true };
    if (widget === undefined || otherWidget === undefined) { return false; }

    for (let keyIndex = 0; keyIndex < keys.length; keyIndex++) {
        const key = keys[keyIndex];
        if (widget[key] !== otherWidget[key]) { return false; }
    }

    return true;
}

export const layoutsEqual = (layout, otherLayout) => {
    if (layout === otherLayout) { return true };
    if (layout === undefined || otherLayout === undefined) { return false; }
    if (layout.length !== otherLayout.length) { return false };

    sortLayout(layout);
    sortLayout(otherLayout);

    for (let index = 0; index < layout.length; index++) {
        if (layout[index] === otherLayout[index]) { continue; };

        if (!widgetsEqual(layout[index], otherLayout[index])) { return false };

    }

    return true;
}

export const convertWidgetsToLayout = widgets => {
    return widgets.map(widget => ({
        ...widget,
        position: {
            'i': `${widget.id}`, // rgl requires id to be a string
            'w': widget.width,
            'h': widget.height,
            'x': widget.x,
            'y': widget.y
        }
    }));
}

export const convertLayoutToWidgets = layout => {
    return layout.map(widget => ({
        id: Number(widget.i),
        width: widget.w,
        height: widget.h,
        x: widget.x,
        y: widget.y
    }));
}