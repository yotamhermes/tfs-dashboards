import { DashboardsActions } from 'actions';

const initialState = {
    selectedId: 34,
    editMode: false
};

const dashboards = (state = initialState, action) => {
    switch (action.type) {
        case DashboardsActions.UPDATE_SELECTED:
            return {
                ...state,
                selectedId: action.payload.selectedId
            }
        case DashboardsActions.UPDATE_EDIT_MODE:
            return {
                ...state,
                editMode: action.payload.editMode
            }
        default:
            return state
    }
};

export default dashboards;