import { combineReducers } from 'redux';
import { dashboards } from './dashboards';

export default combineReducers({
  dashboards
})