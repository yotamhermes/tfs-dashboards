import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#5eb8ff',
            main: '#0288d1',
            dark: '#005b9f'
        },
    },
});

export default () => theme