import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import Dashboard from './Dashboard'
import { getWidgets, updateDashboardWidgets, deleteWidget } from 'api/widgets';

const DashboardContainer = ({ selectedId, editMode }) => {
    const [widgets, setWidgets] = useState();

    useEffect(() => {
        getWidgets(selectedId)
            .then(widgets => setWidgets(widgets));
    }, [selectedId])

    const handleLayoutChange = newWidgets => {
        updateDashboardWidgets(selectedId, newWidgets);
    }

    const handleDeleteWidget = id => {
        const item = widgets.find(x => x.id === id);
        setWidgets(widgets.filter(x => x.id !== id));

        deleteWidget(id)
            .catch(() => widgets.push(item));
    }

    return (
        (!!widgets) &&
        <Dashboard
            editMode={editMode}
            onDeleteWidget={handleDeleteWidget}
            widgets={widgets}
            onLayoutChange={handleLayoutChange} />
    )
}

const mapStateToProps = (state) => ({
    selectedId: state.dashboards.selectedId,
    editMode: state.dashboards.editMode
})

export default connect(mapStateToProps)(DashboardContainer)
