import React from 'react'
import { Widget } from 'common';
import { withStyles } from '@material-ui/styles';
import GridLayout, { WidthProvider } from 'react-grid-layout';
import { layoutUtils } from 'utils';
import clsx from 'clsx';
import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';

const Layout = WidthProvider(GridLayout);

const Dashboards = ({ classes, widgets, onLayoutChange, onDeleteWidget ,editMode }) => {
    const layout = layoutUtils.convertWidgetsToLayout(widgets);

    const handleLayoutChange = (newLayout) => {
        // rgl has an issue - onLayoutChange invoked too many times (Also on the first render)
        if (!layoutUtils.layoutsEqual(layout, newLayout)) {
            const widgets = layoutUtils.convertLayoutToWidgets(newLayout);

            onLayoutChange(widgets);
        }
    };

    const handleDeleteWidget = (e, id) => {
        e.preventDefault();
        e.stopPropagation();

        onDeleteWidget(id);
    }

    const rootClassName = clsx(classes.root, {
        [classes.editBackground]: editMode
    })

    const layoutClassName =  clsx('layout', classes.layout);

    return (
        <div
            className={rootClassName}>
            <Layout
                onLayoutChange={handleLayoutChange}
                className={layoutClassName}
                cols={6}
                rowHeight={80}
                isResizable={editMode}
                isDraggable={editMode}
                containerPadding={[32, 32]}>
                {
                    layout.map(x =>
                        <Widget
                            key={x.position.i}
                            onDelete={editMode ? e => handleDeleteWidget(e, x.id) : null}
                            data-grid={x.position}
                            title={x.name} />
                    )
                }
            </Layout>
        </div>
    )
}

const styles = {
    root: {
        height: '100%',
        overflowY: 'scroll',
        overflowX: 'hidden',
        position: 'relative',
        direction: 'rtl'
    },
    editBackground: {
        backgroundImage: 'radial-gradient(white 1px, transparent 0)',
        backgroundPosition: '-19px -19px',
        backgroundSize: '40px 40px'
    },
    layout: {
        direction: 'ltr'
    },
    editActions: {
        position: 'fixed',
        bottom: 0,
        left: 0,
        margin: 32,
        cursor: 'pointer'
    }
}

export default withStyles(styles)(Dashboards);

