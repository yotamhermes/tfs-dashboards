import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import AppBar from './AppBar'
import { getDashboard } from 'api/dashboards';
import { updateEditMode } from 'actions';

const AppBarContainer = ({ selectedId, editMode, updateEditMode }) => {
    const [dashboard, setDashboard] = useState({})

    useEffect(() => {
        getDashboard(selectedId)
            .then(dashboard => setDashboard(dashboard));
    }, [selectedId])

    return (
        <AppBar
            title={dashboard.name}
            editMode={editMode}
            updateEditMode={updateEditMode}
        />
    )
}

const mapStateToProps = (state) => ({
    selectedId: state.dashboards.selectedId,
    editMode: state.dashboards.editMode
});

const mapDispatchToProps = dispatch => ({
    updateEditMode: editMode => dispatch(updateEditMode(editMode))
});

export default connect(mapStateToProps, mapDispatchToProps)(AppBarContainer)
