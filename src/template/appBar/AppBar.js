import React from 'react'
import MuiAppBar from '@material-ui/core/AppBar';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import MuiIcon from '@material-ui/core/Icon';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiTooltip from '@material-ui/core/Tooltip';

const AppBar = ({ title, classes, editMode, updateEditMode }) => {
    const appBarClasses = {
        root: classes.root
    }
    const actionTooltip = editMode ? 'סיים' : 'ערוך'

    return (
        <MuiAppBar
            position='static'
            classes={appBarClasses}>
            <Typography variant='h4'>
                {title}
            </Typography>
            <MuiTooltip title={actionTooltip}>
                <MuiIconButton
                    onClick={() => updateEditMode(!editMode)}
                    className={classes.actions}>
                    <MuiIcon>
                        {
                            editMode ? 'done' : 'edit'
                        }
                    </MuiIcon>
                </MuiIconButton>
            </MuiTooltip>
        </MuiAppBar>
    )
}

const styles = theme => ({
    root: {
        height: theme.spacing(10),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        padding: 16,
        backgroundColor: '#202020',
        position: 'absolute',
        direction: 'rtl'
    },
    actions: {
        fontSize: 24,
        color: 'white',
        marginLeft: 32
    }
})

export default withStyles(styles)(AppBar);
