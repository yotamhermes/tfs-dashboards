import React, { useState } from 'react';
import MuiDrawer from '@material-ui/core/Drawer';
import { withStyles } from '@material-ui/core/styles';
import MuiList from '@material-ui/core/List';
import MenuItem from './MenuItem';
import clsx from 'clsx';

const AppMenu = ({ classes, items, selectedId, onSelect, ...otherProps }) => {
    const [open, setOpen] = useState(false);

    const hanelOpen = () => {
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
    }

    const paperProps = {
        elevation: 8
    }

    const className = clsx(classes.drawer, {
        [classes.drawerOpen]: open,
        [classes.drawerClose]: !open,
    });

    const drawerClasses = {
        paper: clsx(classes.paper, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
        }),
    };

    return (
        <MuiDrawer
            PaperProps={paperProps}
            onMouseOver={hanelOpen}
            onMouseLeave={handleClose}
            anchor='right'
            variant='permanent'
            className={className}
            classes={drawerClasses}>
            <MuiList>
                {
                    items && items.map(item =>
                        <MenuItem
                            key={item.id}
                            onClick={onSelect}
                            open
                            {...item} 
                            selected={item.id === selectedId} />
                    )
                }
            </MuiList>
        </MuiDrawer>
    )
}

const drawerWidth = 280

const styles = theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    paper: {
        padding: 0,
        top: theme.spacing(10),
        zIndex: 1,
        backgroundColor: '#202020'
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(8) + 1,
        },
    },
})

export default withStyles(styles)(AppMenu);