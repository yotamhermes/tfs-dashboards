import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MuiListItem from '@material-ui/core/ListItem';
import MuiIcon from '@material-ui/core/Icon';
import MuiListItemIcon from '@material-ui/core/ListItemIcon';
import MuiListItemText from '@material-ui/core/ListItemText';
import clsx from 'clsx';
import { useHistory } from 'react-router-dom';

const MenuItem = ({ classes, id, name, icon, selected, open, onClick, ...otherProps }) => {
    const history = useHistory();

    const handleClick = () => {
        onClick(id);
        history.push(`/dashboards/${id}`)
    }

    const listItemClasses = {
        root: classes.root,
        selected: classes.selected
    }

    const iconClassName = clsx(classes.icon, {
        [classes.selectedIcon]: selected,
    })

    const primaryTypographyProps = {
        variant: 'h6'
    }

    return (
        <MuiListItem
            key={id}
            classes={listItemClasses}
            selected={selected}
            button
            onClick={handleClick}>
            <MuiListItemIcon>
                <MuiIcon className={iconClassName}>{icon}</MuiIcon>
            </MuiListItemIcon>
            {
                open && <MuiListItemText
                    primaryTypographyProps={primaryTypographyProps}
                    primary={name} />
            }
        </MuiListItem>
    )
}


const styles = theme => ({
    root: {
        paddingRight: 16,
        fontSize: 800,
        color: 'white',
        direction: 'rtl'
    },
    icon: {
        color: 'white',
        fontSize: 28
    },
    selectedIcon: {
        color: theme.palette.primary.light
    },
    selected: {
        borderRight: '2px solid' + theme.palette.primary.light,
        color: theme.palette.primary.light
    },
})

export default withStyles(styles)(MenuItem);