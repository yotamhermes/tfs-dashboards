import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import AppMenu from './AppMenu'
import { getDashboards } from 'api/dashboards';
import { updateSelected, updateEditMode } from 'actions';

const AppMenuContainer = ({ onSelect, updateEditMode, ...otherProps }) => {
    const [dashboards, setDashboards] = useState([])

    const handleSelect = (...args) => {
        onSelect(...args);
        updateEditMode(false);
    }

    useEffect(() => {
        getDashboards()
            .then(dashboards=> setDashboards(dashboards));
    }, [])

    return (
        <AppMenu 
            onSelect={handleSelect}
            items={dashboards}
            {...otherProps} />
    )
}

const mapStateToProps = (state) => ({
    selectedId: state.dashboards.selectedId
})

const mapDispatchToProps = dispatch => ({
    onSelect: id => dispatch(updateSelected(id)),
    updateEditMode: editMode => dispatch(updateEditMode(editMode))
})

export default connect(mapStateToProps, mapDispatchToProps)(AppMenuContainer)
