import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const Content = ({ classes, children }) => (
    <div className={classes.root}>
        {children}
    </div>
);

const styles = theme => ({
  root: {
    paddingRight: theme.spacing(8) + 1,
    paddingTop: theme.spacing(10),
    height: '100%',
    boxSizing: 'border-box',
    overflow: 'hidden'
  }
})

export default withStyles(styles)(Content);
