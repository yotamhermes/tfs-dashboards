export const DashboardsActions = {
    UPDATE_SELECTED: 'UPDATE_SELECTED',
    UPDATE_EDIT_MODE: 'UPDATE_EDIT_MODE'
};

export const updateSelected = selectedId => ({
    type: DashboardsActions.UPDATE_SELECTED,
    payload: {
        selectedId
    }
})

export const updateEditMode = editMode => ({
    type: DashboardsActions.UPDATE_EDIT_MODE,
    payload: {
        editMode
    }
})

