import arrToObj from 'array-to-object';

let widgetsDB = [
    {
        id: 23,
        dashboardId: 34,
        name: 'זמן עד סוף החודש',
        width: 1,
        height: 2,
        x: 2,
        y: 5
    },
    {
        id: 47,
        dashboardId: 34,
        name: 'מדד הכנסות לעומת יעד',
        width: 3,
        height: 7,
        x: 3,
        y: 0
    },
    {
        id: 12,
        dashboardId: 34,
        name: 'מגמת הכנסות מתחילת החודש',
        width: 3,
        height: 2,
        x: 0,
        y: 0
    },
    {
        id: 15,
        dashboardId: 34,
        name: 'זמן מכירה ממוצע',
        width: 2,
        height: 5,
        x: 0,
        y: 2,
    },
    {
        id: 19,
        dashboardId: 34,
        name: 'מלך המכירות',
        width: 1,
        height: 3,
        x: 2,
        y: 2,
    },
    {
        id: 67,
        dashboardId: 12,
        name: 'משימות פתוחות',
        width: 1,
        height: 2,
        x: 4,
        y: 3
    },
    {
        id: 97,
        dashboardId: 12,
        name: 'באגים',
        width: 2,
        height: 6,
        x: 0,
        y: 2
    },
    {
        id: 54,
        dashboardId: 12,
        name: 'יעדי ספרינט',
        width: 2,
        height: 5,
        x: 2,
        y: 2
    },
    {
        id: 37,
        dashboardId: 12,
        name: 'מלך הספרינט',
        width: 2,
        height: 3,
        x: 4,
        y: 0
    },
    {
        id: 105,
        dashboardId: 4,
        name: 'כמות משתמשים שבועית',
        width: 1,
        height: 2,
        x: 4,
        y: 0
    },
    {
        id: 107,
        dashboardId: 4,
        name: 'יעדי ספרינט',
        width: 2,
        height: 8,
        x: 2,
        y: 0
    },
    {
        id: 100,
        dashboardId: 4,
        name: 'מלך הספרינט',
        width: 1,
        height: 5,
        x: 1,
        y: 0
    }
]

export const getWidgets = (dashboardId) => {
    return new Promise(resolve => {
        const result = widgetsDB.filter(x => x.dashboardId === dashboardId)

        setTimeout(() => resolve(result), Math.floor(Math.random() * 500) + 500);
    });
};

let seq = widgetsDB.reduce((acc, i) => acc > i.id ? acc : i.id, -1);

export const addWidget = (widget) => {
    throw 'not implemented'
}

export const updateDashboardWidgets = (dashboardId, newWidgets) => {
    return new Promise(resolve => {
        const values = newWidgets;
        const keys = values.map(x => x.id);

        var widgetsDictionary = arrToObj(keys, values);

        widgetsDB = widgetsDB.map(x => {
            if (x.dashboardId === dashboardId) {
                return {
                    ...x,
                    ...widgetsDictionary[x.id]
                }
            }

            return x;
        })

        setTimeout(() => resolve(), Math.floor(Math.random() * 500) + 500);
    });
}

export const deleteWidget = (widgetId) => {
    return new Promise(resolve => {

        widgetsDB = widgetsDB.filter(x => x.id !== widgetId);
        setTimeout(() => resolve(), Math.floor(Math.random() * 500) + 500);
    });
};