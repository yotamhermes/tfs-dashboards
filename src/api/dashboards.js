let dashboardsDB = [
    {
        id: 34,
        name: 'הכנסות חודשיות',
        icon: 'trending_up'
    },
    {
        id: 12,
        name: 'ספרינט 12',
        icon: 'assignment'
    },
    {
        id: 4,
        name: 'שימושיות',
        icon: 'data_usage'
    }
]

export const getDashboards = () => {
    return new Promise(resolve => {
        const result = dashboardsDB

        setTimeout(() => resolve(result),  Math.floor(Math.random() * 500) + 500);
    });
};

export const getDashboard = (id) => {
    return new Promise(resolve => {
        const result = dashboardsDB.find(x => x.id === id)

        setTimeout(() => resolve(result),  Math.floor(Math.random() * 500) + 500);
    });
};
