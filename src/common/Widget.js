import React from 'react'
import MuiCard from '@material-ui/core/Card';
import MuiCardHeader from '@material-ui/core/CardHeader';
import { withStyles } from '@material-ui/styles';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiIcon from '@material-ui/core/Icon';

const Widget = ({ title, actions, classes, children, onDelete, ...otherProps }) => {
    const cardClasses = {
        root: classes.root
    };

    const headerClasses = {
        root: classes.header,
        subheader: classes.subheader
    }

    return (
        <MuiCard
            variant='outlined'
            classes={cardClasses}
            {...otherProps}>
            <MuiCardHeader
                classes={headerClasses}
                title={title}
                action={
                    onDelete &&
                    <MuiIconButton
                        onMouseDown={onDelete}
                        className={classes.action}>
                        <MuiIcon className={classes.icon}>
                            delete
                        </MuiIcon>
                    </MuiIconButton>
                }
            />
            {children}
        </MuiCard>
    )
}

const styles = {
    root: {
        direction: 'rtl',
        color: 'white',
        backgroundColor: '#424242'
    },
    header: {
        color: 'white'
    },
    subheader: {
        color: 'rgba(255, 255, 255, 0.7)'
    },
    action: {
        color: 'white'
    },
    icon: {
        fontSize: 24
    }
}

export default withStyles(styles)(Widget);