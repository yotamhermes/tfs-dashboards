import React from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
import MuiTextField from '@material-ui/core/TextField';

const createInputLabelClasses = makeStyles(() => createStyles({
    formControl: {
        right: 0,
        left: 'initial'
    }
}));

const TextField = (props) => {
    const inputLabelProps = {
        classes: createInputLabelClasses()
    };

    return (
        <MuiTextField
            InputLabelProps={inputLabelProps}
            {...props}
        />
    );
}

export default TextField;