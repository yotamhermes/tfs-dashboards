import React from 'react';
import { AppBar, AppMenu, Content } from './template';
import { withStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import routes from './routes';

const App = ({ classes }) => (
  <Router>
    <div className={classes.root}>
      <AppBar />
      <AppMenu />
      <Content>
        <Switch>
          {
            routes.map((route, i) => (
              <Route key={i} {...route} />
            ))
          }
        </Switch>
      </Content>
    </div>
  </Router>
);

const styles = {
  root: {
    backgroundColor: '#181818',
    height: '100vh',
    color: 'white'
  }
};

export default withStyles(styles)(App);
